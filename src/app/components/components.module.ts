import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SlidesComponent} from "./slides/slides.component";
import {LogoComponent} from "./logo/logo.component";
import {IonicModule} from "@ionic/angular";
import {StartComponent} from "./start/start.component";
import {MenuComponent} from "./menu/menu.component";

@NgModule({
    declarations: [SlidesComponent, LogoComponent, StartComponent, MenuComponent],
    exports: [SlidesComponent, LogoComponent, StartComponent, MenuComponent],
    imports: [
        CommonModule,
        IonicModule
    ]
})
export class ComponentsModule {
}
